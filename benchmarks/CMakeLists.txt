#####
# Let us create some benchmark apps we can use for our report

# Benchmark our vector
add_subdirectory(benchmark_01)
add_dependencies(benchmark_01 mylibrary)

# Benchmark our Shell sort algorithm
add_subdirectory(benchmark_02)
add_dependencies(benchmark_02 mylibrary)

# Benchmark our selection sort algorithm
add_subdirectory(benchmark_03)
add_dependencies(benchmark_03 mylibrary)

# Benchmark our selection
add_subdirectory(benchmark_04)
add_dependencies(benchmark_04 mylibrary)
