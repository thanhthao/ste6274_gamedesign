#ifndef HEAP_SORT_H
#define HEAP_SORT_H

#include <functional>
#include <utility>  //swap

namespace mylib {

template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
void max_heapify ( RandomAccessIterator first,int parent,int n, Compare cmp = Compare() ) {

    using size_type = typename RandomAccessIterator::difference_type;
    size_type child {2*parent};
    while( child <= n){

        if((child+1) <= n && cmp(first[child],first[child+1]))
            child++;
        if(cmp(first[parent],first[child]))
            std::swap(first[child], first[parent]);

        parent = child;
        child  = 2*parent;
    }
}


template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
void build_max_heap ( RandomAccessIterator first, RandomAccessIterator last, Compare cmp = Compare() ) {
    using size_type = typename RandomAccessIterator::difference_type;
    size_type n {last - first};

    for( size_type i {n/2}; i >= 1; i-- ) {
         max_heapify(first,i,n,cmp);
    }
}


template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
void heap_sort ( RandomAccessIterator first, RandomAccessIterator last, Compare cmp = Compare() ) {

    using size_type = typename RandomAccessIterator::difference_type;
    size_type n {last - first};

    build_max_heap(first,last,cmp);

    for( size_type i {n}; i >= 2; i-- ) {
        std::swap( first[i], first[1] );
        max_heapify(first,1,i-1,cmp);
    }

}
}

#endif // HEAP_SORT_H



