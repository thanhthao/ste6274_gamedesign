#ifndef LIST_H
#define LIST_H

#include <initializer_list>
#include <list>
namespace mylib {

class List {
public:
    using size_type  = std::size_t;
    using value_type = int;
    List();
    List( std::initializer_list<value_type> list );
    List( const List& list );
    List( List&& list );
    ~List();

    value_type* begin();
    value_type* end();

    value_type* cbegin();
    value_type* cend();

    bool IsEmpty();
    size_type size() const;

    value_type front();
    value_type back();

    void push_back(const value_type& i);
    void push_front(const value_type& t);
    void printElement();
    void setSize(int s);
private:
    size_type _size;                   //the number of element
    value_type *_elem;                  //elem points to an array of _size integer


}; // END class List

}  // END namespace mylib

#endif // LIST_H
