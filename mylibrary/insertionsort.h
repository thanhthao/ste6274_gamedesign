#ifndef INSERTION_SORT_H
#define INSERTION_SORT_H

#include <functional>
namespace mylib {

template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
void insertion_sort ( RandomAccessIterator first, RandomAccessIterator last, Compare cmp = Compare() ) {

    using size_type = typename RandomAccessIterator::difference_type;
    size_type n {last - first};

    for (size_type i {0}; i < n; ++i)
    {
        size_type   value = first[i];
        size_type   j = i;
        for ( ; j > 0 && cmp(value,first[j-1]); j--)
        {
            first[j] = first[j-1];
        }
        first[j] = value;
    }
}
}

#endif // INSERTION_SORT_H


//for( size_type i {1}; i < n; ++i ) {

//   size_type j {i};
//   size_type k {j-1};
//   while(j > 0 && cmp(first[j],first[k])){

//        std::swap(first[j],first[k]);
//         j--;
//    }
// }



