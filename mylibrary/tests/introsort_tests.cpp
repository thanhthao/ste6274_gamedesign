#include<H:/subjects/game_new/VD/gtest-1.6.0/include/gtest/gtest.h>

#include "../introsort.h"

#include <vector>
// //////////  CHANGE


// Helper function
template <typename T>
::testing::AssertionResult VectorsMatch( const std::vector<T>& expected, const std::vector<T>& actual ){

  // Test size
  if( expected.size() != actual.size() )
    return ::testing::AssertionFailure() << "size mismatch: |vector| (" << actual.size() << ") != |expected| (" << expected.size() << ")";

  // Test per element content
  for( typename std::vector<T>::size_type i {0}; i < expected.size(); ++i )
    if( expected.at(i) != actual.at(i) )
      return ::testing::AssertionFailure() << "vector[" << i
                                           << "] (" << actual.at(i) << ") != expected[" << i
                                           << "] (" << expected.at(i) << ")";

  return ::testing::AssertionSuccess();
}


// Vector conatiner initialization tests
TEST(Algorithm_IntroSort,BasicSort) {

  std::initializer_list<int> gold{1,2,3,4,5,6,7};
  std::initializer_list<int> data{7,1,4,3,6,2,5};

  std::vector<int> vec_gold{gold};

  std::vector<int> vec{data};
  mylib::introsort(vec.begin(),vec.end());

  EXPECT_TRUE( VectorsMatch(vec_gold, vec) );

}


// Vector conatiner initialization tests
TEST(Algorithm_IntroSort,BasicSortNegativeValues) {

  std::initializer_list<int> gold{-3,-2,-1,4,5,6,7};
  std::initializer_list<int> data{7,-1,4,-3,6,-2,5};

  std::vector<int> vec_gold{gold};

  std::vector<int> vec{data};
  mylib::introsort(vec.begin(),vec.end());

  EXPECT_TRUE( VectorsMatch(vec_gold, vec) );

}
