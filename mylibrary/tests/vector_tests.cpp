#include <H:/subjects/game_new/VD/gtest-1.6.0/include/gtest/gtest.h>

#include "../vector.h"

#include <vector>
#include <algorithm>






// Helper function
template <typename T>
::testing::AssertionResult VectorsMatch( const std::vector<T>& expected, const mylib::Vector<T>& actual ){

  // Test size
  if( expected.size() != actual.size() ) 
    return ::testing::AssertionFailure() << "size mismatch: |vector| (" << actual.size() << ") != |expected| (" << expected.size() << ")";
  
  // Test per element content
  for( typename std::vector<T>::size_type i {0}; i < expected.size(); ++i )
    if( expected.at(i) != actual.at(i) )
      return ::testing::AssertionFailure() << "vector[" << i
                                           << "] (" << actual.at(i) << ") != expected[" << i
                                           << "] (" << expected.at(i) << ")";

  return ::testing::AssertionSuccess();
}







// Vector conatiner initialization tests
TEST(Container_Vector,Initialization) {

  // Define gold
  size_t                     gold_size {4};
  std::initializer_list<int> gold_lst {1,2,3,4};
  std::vector<int>           gold {gold_lst};


  // Construct 
  mylib::Vector<int> vec {gold_lst};


  // Some size tests
  EXPECT_EQ   (gold_size,vec.size());
  EXPECT_TRUE (VectorsMatch(gold,vec));
}


// Vector move constructor tests
TEST(Container_Vector,Move) {
  
  // Define gold
  size_t                     gold_size {4};
  std::initializer_list<int> gold_lst {1,2,3,4};
  std::vector<int>           gold {gold_lst};


  // Construct 
  mylib::Vector<int> vec {gold_lst};

  // Pre-move tests
  EXPECT_EQ   (gold_size,vec.size());
  EXPECT_TRUE (VectorsMatch(gold,vec));


  // Grab content of vec
  mylib::Vector<int> vec2 { std::move(vec) };


  // Post-move tests
  EXPECT_EQ   (0,vec.size());
  EXPECT_EQ   (gold_size,vec2.size());
  EXPECT_TRUE (VectorsMatch(gold,vec2));
}


// Vector iterator tests
TEST(Container_Vector,Iterator) {
  
  // Define gold
  std::initializer_list<int> gold_lst {4,1,3,2};
  std::initializer_list<int> gold_lst_sorted {1,2,3,4};
  std::initializer_list<int> gold_lst_sorted_reverse {4,3,2,1};
  std::vector<int>           gold_sorted {gold_lst_sorted};
  std::vector<int>           gold_sorted_reverse {gold_lst_sorted_reverse};

  
  // Construct
  mylib::Vector<int> vec {gold_lst};

  // TEST: Sort the vector using < comparison (default)
  std::sort(vec.begin(),vec.end());
  EXPECT_TRUE (VectorsMatch(gold_sorted,vec));

  // TEST: Sort the vector using > comparison
  std::sort(vec.begin(),vec.end(),std::greater<int>());
  EXPECT_TRUE (VectorsMatch(gold_sorted_reverse,vec));
  
}


// Vector push_back test
TEST(Container_Vector,push_back) {

  // Define gold
  std::initializer_list<int> gold_lst {1,2,3,4};
  std::vector<int>           gold {gold_lst};

  
  // Construct and fill
  mylib::Vector<int> vec {};
  for( const auto& element : gold_lst )
    vec.push_back(element);

  // Test content to gold
  EXPECT_TRUE (VectorsMatch(gold,vec));
}


// Vector range tests
TEST(Container_Vector,Range) {

  // Define gold
  std::initializer_list<int> gold_lst {1,2,3,4};

  
  // Construct
  mylib::Vector<int> vec {gold_lst};
 
  // Access inside range
  try {
    (void)vec.at(2); // Access the vec at index 2
    EXPECT_TRUE( ::testing::AssertionSuccess() );
  }
  catch( const std::out_of_range& e ) {
    // RETURN FAILED
    EXPECT_TRUE( ::testing::AssertionFailure() << "Out of range: " << e.what() );
  }
  
  // Tempt faith and try and access outside range: [i] < 0
  try {
    (void)vec.at(-2);
    EXPECT_FALSE( ::testing::AssertionSuccess() << "Not out of range!" );
  }
  catch( const std::out_of_range& e ) {
    
    EXPECT_FALSE( ::testing::AssertionFailure() );
  }

  // Tempt faith and try and access outside range: [i] == size()
  try {
    (void)vec.at(vec.size());
    EXPECT_FALSE( ::testing::AssertionSuccess() << "Not out of range!" );
  }
  catch( const std::out_of_range& e ) {
    
    EXPECT_FALSE( ::testing::AssertionFailure() );
  }

  // Tempt faith and try and access outside range: [i] > size()
  try {
    (void)vec.at(vec.size()+2);
    EXPECT_FALSE( ::testing::AssertionSuccess() << "Not out of range!" );
  }
  catch( const std::out_of_range& e ) {
    
    EXPECT_FALSE( ::testing::AssertionFailure() );
  }
  
}

