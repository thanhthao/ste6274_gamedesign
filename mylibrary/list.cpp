
#include "list.h"
#include <iterator>
#include <algorithm>
#include <iostream>
namespace mylib {

List::List(){
    _size = 0;
    _elem = nullptr;
}


List::List(std::initializer_list<value_type> list)
    : _size {list.size()},_elem{new int[list.size()]} {

    std::copy(list.begin(),list.end(),_elem);                                    //copy from list to elem

}

                                                                             //copy constructor             and allocate space for elements
List::List( const List& list ):_elem{new int[list.size()]},_size{list.size()} {

    std::copy(list._elem,list._elem + _size, _elem);
}
                                                                             //move constructor          " grap the element" of list
List::List( List&& list ):_elem{nullptr},_size{0} {

                                                                             // Copy the data pointer and its length from the source object.
    _size = list._size;
    _elem  = list._elem;
                                                                             // Release the data pointer from the source object so that
                                                                             // the destructor does not free the memory multiple times.
    list._size=0;
    list._elem=nullptr;

}

List::~List(){
    if(_elem != nullptr)
        delete []_elem;
}

List::value_type* List::begin(){
    return _size ? &_elem[0] : nullptr;                                       //pointer to the first element or nullptr
    }

List::value_type* List::end(){
    return _size ? &_elem[_size-1] : nullptr;                                       //pointer to the first element or nullptr
}


                                                                                 //Returns a const_iterator pointing to the first element
List::value_type* List::cbegin(){
    return _size ? &_elem[0] : nullptr;
    }

                                                                                  //Returns a const_iterator pointing to the past-the-end element
List::value_type* List::cend(){
    return _size ? &_elem[_size-1] : nullptr;                                       //pointer to the first element or nullptr
}


bool List::IsEmpty(){
    return _size == 0;
}

List::size_type
List::size() const{
    return _size;
}


                                                                                //front: returns a reference to the first element in the list container.
List::value_type List::front(){
    return _elem[0];
}

                                                                                 //back: returns a reference to the last element in the list container.
List::value_type List::back(){

    return _elem[_size-1];
}

void List::setSize(int s){
    _size = s;
}
                                                                                //add one element in the end of list container
void List::push_back(const value_type &i){
    _size++;
    _elem[_size-1]= i;

}


void List::printElement(){

    std::cout<<"List size: "<<_size<<std::endl;

    for(unsigned int i = 0 ; i < _size ; ++i)
        std::cout<<_elem[i]<<" ";

}

} // END namespace mylib











//add one element in front of list container
//void List::push_front(const value_type &t){

//    List l;
//    l.setSize(_size);
//    for(unsigned int i = 1 ; i < _size ; ++i)
//    {
//        l._elem[i]=_elem[i];
//    }

//    _size++;
//    _elem[0] = t;
//    for(unsigned int i = 1 ; i < _size; ++i)
//        _elem[i] = l._elem[i-1];
//}
