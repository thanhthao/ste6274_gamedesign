#ifndef INTRO_SORT_H
#define INTRO_SORT_H

#include <functional>
#include <algorithm>
#include <math.h>    // floor :round up value
#include <cmath>

#include "shellsort.h"
#include "selectionsort.h"
#include "insertionsort.h"
#include "heapsort.h"

namespace mylib {

template <class RandomAccessIterator>
RandomAccessIterator median_3(RandomAccessIterator first, RandomAccessIterator middle, RandomAccessIterator last){

    if((first >= last && first<=middle) || ( first <= last && first >= middle ))
        return first;
     else if((last >= first && last <= middle )||( last <= first && last >= middle))
           return last;
     else return middle;
}

template <class RandomAccessIterator,class Compare = std::less<typename RandomAccessIterator::value_type>>
void introsort_loop( RandomAccessIterator first, RandomAccessIterator last, int depth_limit,Compare comp = Compare()) {

    int threshold = 2e6;
    while (last-first > threshold){

        if(depth_limit == 0){

            heap_sort(first,last,comp);            //another way for heap_sort()
                                                   // std::make_heap(first,last);
                                                   // std::sort_heap(first,last);
            return;
        }
        --depth_limit;

        auto cut = median_3(first,last,first+(last-first)/2);
        introsort_loop(cut,last,depth_limit,comp);
        last = cut;
    }
} // introsort_loop

template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
void introsort( RandomAccessIterator first, RandomAccessIterator last, Compare comp = Compare() ) {

    introsort_loop(first,last,2*floor(std::log2(last-first)),comp);
    insertion_sort(first,last,comp);
}

} //mylib

#endif // INTRO_SORT_H
