#ifndef BUBBLESORT_H
#define BUBBLESORT_H

#include <functional>
#include <vector>
#include <iterator>
#include <algorithm>
#include <type_traits>
#include <iostream>

namespace mylib {

  template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
  void bubbleSort ( RandomAccessIterator first, RandomAccessIterator last, Compare cmp = Compare() ) {

    using size_type = typename RandomAccessIterator::difference_type;   // Deduce size_type from Random Access Iterator
    const size_type n = last-first;

    for( size_type i = 0; i < n; ++i )
      for( size_type j = 0; j < n-i-1; ++j)
        if( cmp(first[j+1],first[j]) )
          std::swap(first[j+1],first[j]);
  }

}


#endif // BUBBLESORT_H
